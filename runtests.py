import subprocess
import glob
import sys
import minic


QEMUARGS = "qemu-system-arm -M lm3s811evb -serial stdio -nographic -monitor null -semihosting -kernel".split()


def main():
  ret = 0
  match = sys.argv[1] if len(sys.argv) == 2 else "*"
  for fn in glob.glob("test/%s.c" % match):
    outfn = fn + ".bin"
    print "%-16s " % fn,
    ok = minic.main(fn, outfn) == 0
    if ok:
      binsize = len(open(outfn).read())
      print "%3db :" % binsize,
      out = subprocess.check_output(QEMUARGS + [outfn])
    if (not ok) or ("FAIL" in out):
      print "FAIL:"
      print out
      ret = -1
    else:
      print "OK"
  sys.exit(ret)


main()
