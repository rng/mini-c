QEMUARGS=-M lm3s811evb -gdb tcp::3333 -serial stdio -nographic -monitor null -semihosting

wc:
	@wc -l minic.py
	@sloccount minic.py | grep "python:"

%.bin: %.c
	@./minic.py $<

%.dump: %.bin
	@arm-none-eabi-objdump -D -b binary -m arm -Mforce-thumb $<

hello: test/hello.bin
	@qemu-system-arm $(QEMUARGS) -kernel $<

dump: test/hello.dump

test:
	@python runtests.py

convert:
	jupyter nbconvert --to python --template convert.tpl --output-dir out minic.ipynb
	sed -i.bak 's/[ ]*$$//' out/minic.py

clean:
	@rm -rf *.pyc test/*.bin

.PHONY: test dump
