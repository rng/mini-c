// LM3Sxxxx IO Map
#define UART0 (0x4000c000)

// Semihosting commands
#define SYS_EXIT (0x18)

void putc(char c) {
  int *uart = (int*)UART0;
  *uart = c;
}

void puti(int v) {
  char buf[8];
  int i = 7;
  if (v == 0) {
    putc('0');
  } else {
    while (v > 0) {
      buf[i] = (v % 10) + '0';
      i = i - 1;
      v = v / 10;
    }
    while (i < 7) {
      i = i + 1;
      putc(buf[i]);
    }
  }
}

void exit(int n) {
  __svc(SYS_EXIT, n);
}

void assert(int v) {
  if (v == 0) {
     putc('F'); putc('A'); putc('I'); putc('L'); putc('\n');
     exit(0);
  }
}
