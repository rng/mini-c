Mini C Compiler in Python
=========================

A small educational C-subset Compiler to ARM Thumb2 in ~1500 lines of Python. Not to be confused with [TCC][1], the much more impressive small (self compilable) C compiler in C.

## Features:

- (signed) int, char, pointers/arrays thereof, and casts
- arithmetic and comparison ops (`+ - * / % & | >> << > < <= >= == !=`)
- dereference and address operators
- if/else/while
- Function definitions (4 args max) / calls, C-compatible calling convention
- local & global variables

## Usage:
    
```
#!sh
# install deps (for debugging / running locally)
apt-get install qemu-system-arm binutils-arm-none-eabi

# compile and run hello world test program
make hello

# compile and disassemble test program
make dump

# run test suite
make test
```

## Example Phases:

The sections below show the intermediate state of the compiler as it compiles a simple C source file.

### 0. Input Source

```
#!c
int plus1(int n) {
  return n + 1;
}

int main() {
  return plus1(2); 
}
```

### 1. Lexer              (input string -> tokens)

```
#!python
(KEYWORD,  'int')
(ID,       'plus1')
(OPERATOR, '(')
(KEYWORD,  'int')
(ID,       'n')
(OPERATOR, ')')
(OPERATOR, '{')
(KEYWORD,  'return')
(ID,       'n')
(OPERATOR, '+')
(NUM,      1)
(OPERATOR, ';')
(OPERATOR, '}')
(KEYWORD,  'int')
(ID,       'main')
(OPERATOR, '(')
(OPERATOR, ')')
(OPERATOR, '{')
(KEYWORD,  'return')
(ID,       'plus1')
(OPERATOR, '(')
(NUM,      2)
(OPERATOR, ')')
(OPERATOR, ';')
(OPERATOR, '}')
```

### 2. Parser             (tokens -> AST/IR)

```
#!python
[
  Func(
    name='plus1', rtype='int',
    args=[('int', 'n')],
    body=[
      Return(
        expr=BinOp(
               op='+',
               l=Name(v='n', typ=None),
               r=Num(v=1, typ=None),
               typ=None),
        typ=None)]),
  Func(
    name='main', rtype='int',
    args=[],
    body=[
      Return(
        expr=Call(
               func='plus1',
               args=[Num(v=2, typ=None)], typ=None),
        typ=None)])
]   
``` 

### 3. Semantic Analysis  (IR -> IR)

```
#!python
[
  Func(
    name='plus1', rtype='int',
    args=[('int', 'n')],
    body=[
      Return(
        expr=BinOp(
               op='+',
               l=Name(v='n', typ='int'),
               r=Num(v=1, typ='int'),
               typ='int'),
        typ='int')]),
  Func(
    name='main',
    rtype='int', args=[],
    body=[
      Return(
        expr=Call(
               func='plus1',
               args=[Num(v=2, typ='int')], typ='int'),
        typ='int')])
] 
```

### 4. Lowering           (IR -> LIR)

```
plus1:
  entr 4
  stl  0, 0, 4
  ldl  4, 0, 4
  imm  5, 1
  add  4, 5
  mov  0, 4
  jmp  ret2
ret1:
  ret  4
main:
  entr 0
  imm  4, 2
  push 4
  pop  4
  mka  4, 0
  call plus1
  mov  4, 0
  mov  0, 4
  jmp  ret2
ret2:
  ret  0
```

### 5. Peephole Optimiser (LIR -> LIR)

```
plus1:
  entr 4
  stl  0, 0, 4
  ldl  4, 0, 4
  imm  5, 1
  add  4, 5
  mov  0, 4
ret1:
  ret  4
main:
  entr 0
  imm  0, 2
  call plus1
  mov  0, 0
ret2:
  ret  0
```

### 6. Code Generation    (LIR -> ASM)

```
00200020d50000000000000000000000
00000000000000000000000000000000
00000000000000000000000000000000
00000000000000000000000000000000
00000000000000000000000000000000
00000000000000000000000000000000
00000000000000000000000000000000
00000000000000000000000000000000
00000000000000000000000000000000
00000000000000000000000000000000
00000000000000000000000000000000
00000000000000000000000000000000
f0b581b06f4638603c68012564192046
01b0f0bdf0b56f460220fff7f1ff0046
f0bd
```

[1]: https://bellard.org/tcc/
