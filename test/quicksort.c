#include "stdlib.h"

void swap(int *arr, int a, int b) {
    int t = arr[a];
    arr[a] = arr[b];
    arr[b] = t;
}

int partition(int *arr, int lo, int hi) {
  int pivot = arr[hi];
  int i = lo - 1;
  int j = lo;

  while (j < hi) {
    if (arr[j] <= pivot) {
      i = i + 1;
      if (i != j) {
        swap(arr, i, j);
      }
    }
    j = j + 1;
  }
  swap(arr, i+1, hi);
  return i + 1;
}

void quicksort(int *arr, int lo, int hi) {
  if (lo < hi) {
    int p = partition(arr, lo, hi);
    quicksort(arr, lo, p - 1);
    quicksort(arr, p + 1, hi);
  }
}

void main() {
  int arr[5];

  arr[0] = 5;
  arr[1] = 2;
  arr[2] = 9;
  arr[3] = 7;
  arr[4] = 1;

  quicksort(arr, 0, 4);

  assert(arr[0] == 1);
  assert(arr[1] == 2);
  assert(arr[2] == 5);
  assert(arr[3] == 7);
  assert(arr[4] == 9);

  exit(0);
}
