#include "stdlib.h"

void num() {
  if (1) {
  } else {
    assert(0);
  }
  if (0) {
    assert(0);
  } 
}

void cmp() {
  if (1<2) {
  } else {
    assert(0);
  }
  if (2<1) {
    assert(0);
  } else {
  }
}

int min(int a, int b) {
  if (a < b) {
    return a;
  } else {
    return b;
  }
}

void main()
{
  num();
  cmp();
  assert(min(1,2) == 1);
  assert(min(2,1) == 1);
  assert(min(2,2) == 2);
  exit(0);
}

