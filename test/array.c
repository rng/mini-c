#include "stdlib.h"

void simpleint() {
  int arr[3];
  arr[0] = 1;
  arr[1] = 2;
  arr[2] = 3;
  assert(arr[0] == 1);
  assert(arr[1] == 2);
  assert(arr[2] == 3);
}

void simplechar() {
  char arr[3];
  arr[0] = 1;
  arr[1] = 2;
  arr[2] = 3;
  assert(arr[0] == 1);
  assert(arr[1] == 2);
  assert(arr[2] == 3);
}

void memcpy(char *dst, char *src, int count) {
  int i = 0;
  while (i < count) {
    *dst = *src;
    dst = dst + 1;
    src = src + 1;
    i = i + 1;
  }
}

void copytest() {
  char a[3];
  char b[3];
  a[0] = 4;
  a[1] = 5;
  a[2] = 6;
  memcpy(b, a, 3);
  assert(b[0] == 4);
  assert(b[1] == 5);
  assert(b[2] == 6);
}

void main() {
  simpleint();
  simplechar();
  copytest();
  exit(0);
}
