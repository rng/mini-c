#include "stdlib.h"

int main() {
  assert((1>2) == 0);
  assert((2>1) == 1);
  assert((1<2) == 1);
  assert((2<0) == 0);
  assert((1<=2) == 1);
  assert((2<=1) == 0);
  assert((2>=1) == 1);
  assert((2>=3) == 0);
  assert((2==3) == 0);
  assert((3==3) == 1);
  assert((3!=3) == 0);
  assert((3!=2) == 1);
  exit(0);
}
