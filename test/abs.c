#include "stdlib.h"

int abs(int n) {
  if (n > 0) {
    return n;
  } else {
    return -n;
  }
}

void main() {
  assert(abs(0) == 0);
  assert(abs(1) == 1);
  assert(abs(0-1) == 1);
  exit(0);
}
