#include "stdlib.h"

int main() {
  assert((1 - -1) == 2);
  assert((1+2) == 3);
  assert((9+2) == 11);
  assert((5/2) == 2);
  assert((2/5) == 0);
  assert((1-2) == -1);
  assert((4-2) == 2);
  assert((5%2) == 1);
  assert((5%5) == 0);
  assert((2*3) == 6);
  assert((10*10) == 100);
  exit(0);
}
