#include "stdlib.h"

void nothing() {
}

int zero() {
  return 42;
}

int one(int a) {
  return a;
}

int two(int a, int b) {
  return a + b;
}

int three(int a, int b, int c) {
  return a + b + c;
}

int four(int a, int b, int c, int d) {
  return a + b + c + d;
}

int recurse(int n) {
  if (n > 0) {
    return n + recurse(n - 1);
  }
  return 0;
}

int ackermann(int m, int n) {
  if (m == 0) {
    return n+1;
  }
  if (m > 0) {
    if (n == 0) {
      return ackermann(m - 1, 1);
    }
    if (n > 0) {
      return ackermann(m - 1, ackermann(m, n - 1));
    }
  }
}

void main() {
  nothing();
  assert(zero() == 42);
  assert(one(1) == 1);
  assert(two(1,2) == 3);
  assert(three(1,2,3) == 6);
  assert(four(1,2,3,4) == 10);
  assert(recurse(5) == 15);
  assert(ackermann(2, 2) == 7);
  exit(0);
}
