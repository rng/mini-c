#include "stdlib.h"

int foo;
int stuff[4];
char bar;

void main() {
  foo = 42;
  assert(foo == 42);
  bar = 92;
  assert(bar == 92);
  stuff[0] = 5;
  stuff[1] = 6;
  assert(stuff[0] == 5);
  assert(stuff[1] == 6);
  exit(0);
}
