// test

#include "stdlib.h"

void w32(int *addr, int val) {
  *addr = val;
}

int r32(int *addr) {
  return *addr;
}

char arr[8];
int moo;

void nmi_handler() { while (1) {} }
void hardfault_handler() { while (1) {} }

void interrupt_handler0()
{
}

int fact(int n) {
  if (n > 0) {
    return n*fact(n - 1);
  } else {
    return 1;
  }
}

char *cheese(int foo, char bar) {
  return &bar;
}

int cake(int *bar) {
    return *bar;
}

void set_thing(int *val) {
  *val = 0x42;
}

int badger(int x) {
  if (x & (1<<5)) {
    return 3;
  }
  return 2;
}

int thingloop()
{
  char *m;
  int i = 0;
  int foo;
  int bar[8];
  arr[i] = *m;
  set_thing(&foo);
  while (i < 10) {
    w32(&i, (i&42)+2);
    foo = r32(&bar[i]) + 1;
    i = i + foo;
    bar[i] = i*moo;
  }
  return bar[4];
}

void bat() {
  return;
}

int main() {
  //while (1) {
    putc('h');
    putc('e');
    putc('l');
    putc('l');
    putc('o');
    putc(' ');
    puti(1234567);
    putc(' ');
    puti(2 <= 2);
    putc('\n');
  //}
  exit(0);
}
