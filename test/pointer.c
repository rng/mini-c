#include "stdlib.h"

void incr(int *v) {
  *v = *v + 1;
}

void swap(int *a, int *b) {
  int tmp = *a;
  *a = *b;
  *b = tmp;
}

int arith(int ofs) {
  int a = 20;
  int b = 30;
  int *p = &a + ofs;
  return *p;
}

char byteint(int v, int b) {
  char *c = ((char*)&v) + b;
  return *c;
}

void main() {
  int foo = 42;
  int bar = 1;
  assert(foo == 42);
  incr(&foo);
  assert(foo == 43);
  incr(&foo);
  assert(foo == 44);
  swap(&foo, &bar);
  assert(bar == 44);
  assert(foo == 1);
  assert(arith(0) == 20);
  assert(arith(1) == 30);
  assert(byteint(0x12345678, 0) == 0x78);
  assert(byteint(0x12345678, 1) == 0x56);
  assert(byteint(0x12345678, 2) == 0x34);
  assert(byteint(0x12345678, 3) == 0x12);
  exit(0);
}
