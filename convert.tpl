{%- extends 'null.tpl' -%}

{% block header %}#!/usr/bin/env python
# vim:set ts=2 sw=2 sts=2 et: #
{% endblock header %}

{% block in_prompt %}
{% endblock in_prompt %}

{% block input %}
{{ cell.source | ipython2python }}
{% endblock input %}

{% block markdowncell scoped %}
{{ cell.source | wrap_text(width=78) | comment_lines}}
{% endblock markdowncell %}
