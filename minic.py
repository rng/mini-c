#!/usr/bin/env python
# vim:set ts=2 sw=2 sts=2 et: #

# # Mini-C
#
# https://bitbucket.org/rng/mini-c/src
#
# A small educational C-subset Compiler to ARM Thumb2 in ~1500 lines of Python.
# Note this is not an example of how a real (production) compiler works, rather
# the minimal implementation that can compile real programs.
#
# ## Features:
#
# - (signed) int, char, and pointers/arrays thereof
# - arithmetic, comparison and pointer ops
# - if/else/while
# - Function definitions (4 args max) / calls, C-compatible calling convention
# - local, global variables
# - basic type checker and peephole optimiser
#
# ## Overview:
#
# The compiler is made up of 6 phases (and a driver that ties them together):
#
# 1. [Lexer](#lexer)                 (input string -> tokens)
# 2. [Parser](#parser)               (tokens -> AST/IR)
# 3. [Semantic analysis](#semantic)  (IR -> IR)
# 4. [Lowering](#lowering)           (IR -> LIR)
# 5. [Peephole optimiser](#peephole) (LIR -> LIR)
# 6. [Code generation](#codegen)     (LIR -> ASM)
# 7. [Driver](#driver)               (combines all phases above)
#
# Inline code snippets comments are executable in the python prompt.
#
# Comments of the form `# A6.X.Y (T1) XYZ` are references to the
# [ARM v7-M Architecture Reference Manual (ARM DDI 0403C)][1]
# [1]: https://www.pjrc.com/teensy/beta/DDI0403D_arm_architecture_v7m_reference_manual.pdf

import sys
import enum
import subprocess
from collections import namedtuple


# ## 1. Lexer
#
# The lexer (or tokeniser) takes an input string and allows consuming the string
# into (type, value) tokens. Token types are:
#
# - (ID, <identifier string>)
# - (NUM, <integer value>)
# - (OPERATOR, <operator/separator string>)
# - (KEYWORD, <keyword string>)
#
# >>> l = Lexer("if (1) return 2")
# >>> print l.tokt, l.tok
# >>> while l.tok:
# >>>   print l.next()
#     (OPERATOR, '(')
#     (NUM, 1)
#     (OPERATOR, ')'),
#     (KEYWORD, 'return'),
#     (NUM, 2),

class Token(enum.Enum):
  ID, NUM, OPERATOR, KEYWORD = range(4)


class Lexer(object):
  KEYWORDS = "char int void if else while return".split()

  def __init__(self, data):
    self.data, self.p, self.line = data, 0, 0
    self.tok, self.tokt = None, None
    self.next()

  def _eat(self):
    c = self.data[self.p]
    self.p += 1
    return c

  def _eatspace(self):
    """ Consumes all whitespace and single line comments from the current
        position in the input, until a non-whitespace char is reached """
    while self.p < len(self.data):
      if self.data[self.p] in " \t\r\n":
        if self.data[self.p] == "\n":
          self.line += 1
        self.p += 1
      elif ((self.p + 1) < len(self.data) and
            self.data[self.p] == "/" and self.data[self.p + 1] == "/"):
        while self.data[self.p] != "\n":
          self.p += 1
      else:
        break

  def next(self):
    """ Consume sufficient bytes from the input to read a single token.
        Ignores whitespace and comments. """
    self.tok = self.tokt = None
    # consume whitespace
    self._eatspace()
    # if we're past the end of our input, return
    if self.p >= len(self.data):
      return

    # consume a single character to determine token type
    c = self._eat()
    if c.isalpha() or c == "_":
      # ID/KEYWORD: letter or '_' followed by 0+ letters, digits or '_'
      self.tok = c
      while self.data[self.p].isalnum() or self.data[self.p] == "_":
        self.tok += self._eat()
      self.tokt = (Token.KEYWORD if self.tok in self.KEYWORDS else Token.ID)
    elif c.isdigit():
      # NUM: digit followed by hex digits (hack to handle hex & decimal numbers)
      num = c
      while ((self.p < len(self.data)) and
             (self.data[self.p].isdigit() or self.data[self.p] in "xabcdef")):
        num += self._eat()
      num = int(num, 16) if num.startswith("0x") else int(num)
      self.tok, self.tokt = num, Token.NUM
    elif c == "'":
      # NUM (char literal): quotes around a single char (or a \ escaped char)
      escapes = {"n": "\n", "r": "\r", "t": "\t", "\\": "\\"}
      char = self._eat()
      if char == "\\":
        char = escapes[self._eat()]
      self.tok, self.tokt = ord(char), Token.NUM
      assert self._eat() == "'", "Expected '"
    elif c in "()[]{},;~+-*/%":
      # OPERATOR (single character)
      self.tok, self.tokt = c, Token.OPERATOR
    elif c in "+-*/&|!=<>%":
      # OPERATOR (two character)
      op = c
      if self.data[self.p] in "&|!=<>":
        op += self._eat()
      self.tok, self.tokt = op, Token.OPERATOR
    else:
      # Otherwise unhandled character
      raise Exception("unhandled character %r" % c)
    # return token we just consumed
    return self.tokt, self.tok


# ## 2. Parser / Intermediate Representation (IR)
#
# The parser consumes tokens from the lexer and returns an abstract syntax tree
# (AST). Since we do minimal optimisation, the AST is also our intermediate
# representation (IR). IR types defined below, represented as named tuples. The
# parser is a simple recursive-descent parser - parsing is a set of mutually
# recursive functions where each function parses a specific AST type. Each parse
# function consumes tokens from the lexer and returns it's respective IR type.
#
# For example, given the Lexer tokens: (NUM, 1), (OPERATOR, '+'), (NUM, 2), a
# call to parse_expr() would return:
#
#     Bin('+', Num(v=1), Num(v=2))

# >>> lex = Lexer("int f() { if (1) { return 2; } }")
# >>> print Parser(lex).parse()
#     Func(name='f', rtype='int', args=[],
#       body=[
#         If(test=Test(Num(v=1, typ=None), typ=None),
#            tb=[
#              Return(expr=Num(v=2, typ=None), typ=None)
#            ],
#            fb=[])
#       ])

IRName = namedtuple("Name", "v typ")
IRNum = namedtuple("Num", "v typ")
IRCall = namedtuple("Call", "func args typ")
IRVar = namedtuple("Var", "name count typ")
IRUnary = namedtuple("Unary", "op expr typ")
IRBin = namedtuple("BinOp", "op l r typ")
IRTest = namedtuple("Test", "expr typ")
IRCast = namedtuple("Cast", "expr typ")
IRAssign = namedtuple("Assign", "var expr")
IRReturn = namedtuple("Return", "expr typ")
IRIf = namedtuple("If", "test tb fb")
IRWhile = namedtuple("While", "test body")
IRSubscript = namedtuple("Subscript", "name sub typ")
IRFunc = namedtuple("Func", "name rtype args body")


# The grammar for the language is specified below. It is somewhat compatible
# with C, but not an exact match, mostly for simplicity of the parser.
#
#     program   : global*
#     global    : func
#               | var ';'
#     type      : KEYWORD [ '*' ]
#     var       : type ID [ '[' NUM ']' ]
#     arg       : type ID
#     arglist   : arg
#               | arg ',' arglist
#     func      : type ID '(' arg_list ')' block
#     block     : '{' stmt* '}'
#     stmt      : if
#               | while
#               | return ';'
#               | expr ';'
#     expr      : prim '=' expr
#               | prim binop expr
#     binop     : '>' | '>=' | '<=' | '<' | '!=' | '==' | '*' | '/'
#               | '%' | '+' | '-' | '&' | '|' | '<<' | '>>'
#     cast      : '(' type ')' expr
#     callargs  : expr
#               | expr ',' callargs
#     call      : ID '(' callargs ')'
#     subscript : ID '[' expr ']'
#     unop      : '*' | '&' | '-'
#     unary     : unop prim
#     prim      : '(' expr ')'
#               | unary
#               | cast
#               | var
#               | call
#               | subscript
#               | ID
#               | NUM
#     if        : 'if' '(' expr ')' block [ 'else' block ]
#     while     : 'while' '(' expr ')' block
#     return    : 'return' [ expr ]
#
# Now we can define the parser:


class Parser(object):
  def __init__(self, lex):
    self.lex = lex

  def _consume_typ(self, typ):
    """ Consume a token from the lexer, throw an exception if it isn't the type
        expected. eg: `self._consume_typ(Token.ID)`
    """
    if self.lex.tokt != typ:
      raise Exception("Line %d: expected %s got %s ('%s')" % (
                      self.lex.line, typ, self.lex.tokt, self.lex.tok))
    t = self.lex.tok
    self.lex.next()
    return t

  def _consume_val(self, val):
    """ Consume a token from the lexer, throw an exception if it isn't the value
        expected. eg: `self._consume_val("if")`
    """
    if self.lex.tok != val:
      raise Exception("Line %d: expected '%s' got '%s'" % (
                      self.lex.line, val, self.lex.tok))
    t = self.lex.tok
    self.lex.next()
    return t

  def parse(self):
    """ Parse a file returning a list of global declarations (IRFunc / IRVar)
    """
    r = []
    while self.lex.tok is not None:
      r.append(self.parse_global())
    return r

  def parse_global(self):
    typ = self.parse_type()
    name = self._consume_typ(Token.ID)
    if self.lex.tok == "(":
      return self.parse_func(typ, name)
    else:
      v = self.parse_var(typ, name)
      self._consume_val(";")
      return v

  def parse_type(self):
    base = self._consume_typ(Token.KEYWORD)
    if self.lex.tok == "*":
      base += self._consume_val("*")
    return base

  def parse_var(self, typ, name):
    if self.lex.tok == "[":
      self._consume_val("[")
      sz = self._consume_typ(Token.NUM)
      self._consume_val("]")
      return IRVar(name, sz, typ + "[]")
    return IRVar(name, -1, typ)

  def parse_func(self, rtype, name):
    self._consume_val("(")
    args = []
    while self.lex.tok != ")":
      args.append(self.parse_arg())
      if self.lex.tok != ")":
        self._consume_val(",")
    self._consume_val(")")
    body = self.parse_block()
    return IRFunc(name, rtype, args, body)

  def parse_arg(self):
    typ = self.parse_type()
    name = self._consume_typ(Token.ID)
    return typ, name

  def parse_block(self):
    self._consume_val("{")
    stmts = []
    while self.lex.tok != "}":
      stmts.append(self.parse_stmt())
    self._consume_val("}")
    return stmts

  def parse_stmt(self):
    if self.lex.tok == "if":
      r = self.parse_if()
    elif self.lex.tok == "while":
      r = self.parse_while()
    elif self.lex.tok == "return":
      r = self.parse_return()
      self._consume_val(";")
    else:
      r = self.parse_expr()
      self._consume_val(";")
    return r

  def parse_expr(self):
    l = self.parse_prim()
    if self.lex.tok == "=":
      self._consume_val("=")
      r = self.parse_expr()
      return IRAssign(l, r)
    elif self.lex.tok in [">", ">=", "<", "<=", "!=", "=="]:
      op = self._consume_typ(Token.OPERATOR)
      r = self.parse_expr()
      return IRBin(op, l, r, None)
    elif self.lex.tok in ["*", "/", "%", "+", "-", "&", "|", ">>", "<<"]:
      op = self._consume_typ(Token.OPERATOR)
      r = self.parse_expr()
      return IRBin(op, l, r, None)
    return l

  def parse_cast(self):
    t = self.parse_type()
    self._consume_val(")")
    e = self.parse_expr()
    return IRCast(e, t)

  def parse_call(self, name):
    self._consume_val("(")
    args = []
    while self.lex.tok != ")":
      args.append(self.parse_expr())
      if self.lex.tok != ")":
        self._consume_val(",")
    self._consume_val(")")
    return IRCall(name, args, None)

  def parse_subscript(self, name):
    self._consume_val("[")
    v = self.parse_expr()
    self._consume_val("]")
    return IRSubscript(IRName(name, None), v, None)

  def parse_prim(self):
    if self.lex.tok == "(":
      self._consume_val("(")
      if self.lex.tok in ["int", "char"]:
        return self.parse_cast()
      v = self.parse_expr()
      self._consume_val(")")
      return v
    elif self.lex.tok in ["*", "&", "-"]:
      t = self.lex.tok
      self._consume_val(t)
      return IRUnary(t, self.parse_prim(), None)
    elif self.lex.tokt == Token.KEYWORD:
      typ = self.parse_type()
      name = self._consume_typ(Token.ID)
      return self.parse_var(typ, name)
    elif self.lex.tokt == Token.ID:
      name = self._consume_typ(Token.ID)
      if self.lex.tok == "(":
        return self.parse_call(name)
      elif self.lex.tok == "[":
        return self.parse_subscript(name)
      return IRName(name, None)
    else:
      return IRNum(self._consume_typ(Token.NUM), None)

  def parse_test(self):
    return IRTest(self.parse_expr(), None)

  def parse_if(self):
    self._consume_val("if")
    self._consume_val("(")
    test = self.parse_test()
    self._consume_val(")")
    tb = self.parse_block()
    fb = []
    if self.lex.tok == "else":
      self._consume_val("else")
      fb = self.parse_block()
    return IRIf(test, tb, fb)

  def parse_while(self):
    self._consume_val("while")
    self._consume_val("(")
    test = self.parse_test()
    self._consume_val(")")
    tb = self.parse_block()
    return IRWhile(test, tb)

  def parse_return(self):
    self._consume_val("return")
    v = None
    if self.lex.tok != ";":
      v = self.parse_expr()
    return IRReturn(v, None)


# ## 3. Semantic Analysis / Type Checking
#
# The semantic analyser takes a IR and ensures it makes sense semantically:
#
# - Check types match
# - Check argument/variable names exist/match
# - Check function calls match function declarations
#
# In doing so it assigns types to IR nodes, so the return IR is fully typed.
#
# This is done by recursively visiting the IR, building a symbol table (mapping
# of variable name -> type). Similar to the parser, each visitor function checks
# the matching IR node and returns a typed instance of the node. We cheat and
# upcast most types to int, meaning the compiler is more permissive than
# standard C.

# Invalid types:
# >>> Semantic().typecheck([
# >>>   IRFunc(name='f',
# >>>     rtype='int', args=[("char*", "n")],
# >>>     body=[
# >>>       IRReturn(expr=IRName(v='n', typ=None), typ=None)
# >>>     ])
# >>>   ])
#     ...
#     Exception: Unmatched return type 'int' <> 'char*'

# Valid types:
# >>> Semantic().typecheck([
# >>>   IRFunc(name='f',
# >>>     rtype='int', args=[("int", "n")],
# >>>     body=[
# >>>       IRReturn(expr=IRName(v='n', typ=None), typ=None)
# >>>     ])
# >>>   ])
#     Func(name='f',
#       rtype='int', args=[('int', 'n')],
#       body=[
#         Return(expr=Name(v='n', typ='int'), typ='int')
#       ]
#     )

# To start we define some symbol table helper functions:

def lookup(symtab, name):
  """ Lookup a symbol by name in the symbol table returning that symbol's type
      if found. If no symbol is found, check the parent symbol table if present.
  """
  if name in symtab:
    return symtab[name]
  if "*parent*" in symtab:
    return lookup(symtab["*parent*"], name)
  raise Exception("Unknown variable '%s'" % name)


def define(symtab, name, val):
  """ Define a symbol in the symbol table ('val' is a type) """
  if name in symtab:
    raise Exception("Variable '%s' already defined" % name)
  symtab[name] = val


# And also some type checking helpers:


def check_types(info, a, b):
  """ Check types a and b match (after upcasting) """
  def upcast(t):
    t = t.replace("[]", "*")
    return {"char":  "int",
            "char*": "int",
            "int*":  "int"}.get(t, t)
  if upcast(a) != upcast(b):
    raise Exception("Unmatched %s type '%s' <> '%s'" % (info, a, b))


def is_pointer_type(t):
  """ Return true if the given type is a pointer (or array) """
  return t.endswith("*") or t.endswith("[]")


def pointer_scale(expr):
  """ Helper function to build a n<<2 expression for integer pointer arithmetic
  """
  return IRBin("<<", expr, IRNum(2, "int"), "int")


def binop_coerce_type(a, b):
  if is_pointer_type(a): return a
  if is_pointer_type(b): return b
  return a


# And now we can define the semantic analysis visitor:


class Semantic(object):
  def _visit(self, ir, symtab):
    return getattr(self, "visit_" + ir.__class__.__name__)(ir, symtab)

  def _visit_block(self, stmts, symtab):
    return [self._visit(stmt, symtab) for stmt in stmts]

  def typecheck(self, ir):
    # set up initial symbol table with our "intrinsics"
    symtab = {
        "__svc": ["int", "int", "int"] # Supervisor Call - used for semihosting
    }
    return self._visit_block(ir, symtab)

  def visit_Func(self, func, symtab):
    define(symtab, func.name, [typ for (typ, _) in func.args] + [func.rtype])
    symtab = {"*parent*": symtab, "*return*": func.rtype}
    for typ, arg in func.args:
      define(symtab, arg, typ)
    body = self._visit_block(func.body, symtab)
    return IRFunc(func.name, func.rtype, func.args, body)

  def visit_Var(self, var, symtab):
    define(symtab, var.name, var.typ)
    return var

  def visit_Assign(self, assign, symtab):
    expr = self._visit(assign.expr, symtab)
    var = self._visit(assign.var, symtab)
    check_types("assignment", expr.typ, var.typ)
    return IRAssign(var, expr)

  def visit_Name(self, name, symtab):
    typ = lookup(symtab, name.v)
    return IRName(name.v, typ)

  def visit_Num(self, num, symtab):
    return IRNum(num.v, "int")

  def visit_Return(self, retn, symtab):
    expr = None
    typ = "void"
    if retn.expr is not None:
      expr = self._visit(retn.expr, symtab)
      typ = expr.typ
    check_types("return", symtab["*return*"], typ)
    return IRReturn(expr, typ)

  def visit_Unary(self, un, symtab):
    expr = self._visit(un.expr, symtab)
    if un.op == "*":
      assert is_pointer_type(expr.typ), "Expected a pointer type"
      return IRUnary(un.op, expr, expr.typ[:-1])
    elif un.op == "&":
      if not isinstance(un.expr, (IRName, IRSubscript)):
        raise Exception("Can't take address of non-lvalue: %s" % str(un.expr))
      return IRUnary(un.op, expr, expr.typ + "*")
    elif un.op == "-":
      check_types("negation", expr.typ, "int")
      return IRUnary(un.op, expr, expr.typ)
    raise

  def visit_Cast(self, cast, symtab):
    expr = self._visit(cast.expr, symtab)
    return IRCast(expr, cast.typ)

  def visit_Test(self, test, symtab):
    expr = self._visit(test.expr, symtab)
    return IRTest(expr, expr.typ)

  def visit_BinOp(self, binop, symtab):
    lexpr = self._visit(binop.l, symtab)
    rexpr = self._visit(binop.r, symtab)
    check_types("binop", lexpr.typ, rexpr.typ)
    # get the resulting type of this op after coercion
    typ = binop_coerce_type(lexpr.typ, rexpr.typ)
    # if we're doing pointer arithmetic on an int, scale the arithmetic value
    # eg: `int *x; return x + 1;` == `((char*)x) + 4`
    if is_pointer_type(typ) and typ.startswith("int"):
      if lexpr.typ == "int": lexpr = pointer_scale(lexpr)
      if rexpr.typ == "int": rexpr = pointer_scale(rexpr)
    return IRBin(binop.op, lexpr, rexpr, typ)

  def visit_If(self, ife, symtab):
    test = self._visit(ife.test, symtab)
    check_types("if test", test.typ, "int")
    tb = self._visit_block(ife.tb, symtab)
    fb = self._visit_block(ife.fb, symtab)
    return IRIf(test, tb, fb)

  def visit_While(self, whil, symtab):
    test = self._visit(whil.test, symtab)
    check_types("while test", test.typ, "int")
    body = self._visit_block(whil.body, symtab)
    return IRWhile(test, body)

  def visit_Call(self, call, symtab):
    func_type = lookup(symtab, call.func)
    arg_exprs = [self._visit(arg, symtab) for arg in call.args]
    assert len(func_type) - 1 == len(arg_exprs), "Unexpected arguments to call"
    assert len(arg_exprs) <= 4, "Too many arguments to call (max 4)"
    for func_arg_type, call_arg_type in zip(func_type, arg_exprs):
      check_types("call argument", func_arg_type, call_arg_type.typ)
    return IRCall(call.func, arg_exprs, func_type[-1])

  def visit_Subscript(self, subs, symtab):
    array = self._visit(subs.name, symtab)
    subscript = self._visit(subs.sub, symtab)
    typ = array.typ.strip("[]*")
    if typ == "int":
      subscript = pointer_scale(subscript)
    check_types("subscript", subscript.typ, "int")
    assert is_pointer_type(array.typ), "Expected a pointer type for array"
    return IRSubscript(array, subscript, typ)


# ## 4. Lowering / Register Allocator
#
# Lowering takes IR and returns Lowered IR (LIR). LIR is a linear representation
# of the IR very close to assembly. All nested expressions are linearised and
# control flow statements are turned into jumps. Linearisation is performed by
# traversing the IR and emitting LIR instructions. Some target specific details
# are included in the lowering process such as register allocation. Allocation
# is naive using R4-R6 as temporaries. R0-R3 are arguments, R7 is a copy of the
# stack pointer (to allow us to access the SP using most Thumb instructions
# which only have R0-R7 access).
#
# Arguments are copied into locals in the function prolog to simplify saving
# them across calls. Previous R4-R7 are also saved. Thus the stack layout looks
# like:
#
#          | old R4
#          | old R5
#          | old R6
#          | old R7
#          | old LR
#          | saved arg0 (R0)
#      ^   | ...
#      |   | saved argN (RN, max 4 args)
#      |   | local 0
#      :   | ...
#     sp+4 | local N-1
#     sp+0 | local N

# >>> Lower().lower([
# >>>   IRFunc(name='f',
# >>>     rtype='int', args=[("int", "n")],
# >>>     body=[
# >>>       IRReturn(expr=IRName(v='n'))
# >>>     ])
# >>>   ])
#     [('lbl', 'f'),
#      ('entr', 4, 1),
#      ('stl', 0, 0, 4),
#      ('ldl', 4, 0, 4),
#      ('mov', 0, 4),
#      ('jmp', 'ret1'),
#      ('lbl', 'ret1'),
#      ('ret', 4)]

# To start, define some support infrastructure for the lowering process. The
# definitions class tracks mapping of variables to stack/global addresses:


def base_size(typ):
  """ Returns the byte size for the base of a given type (ignore array size)
  """
  if typ.endswith("[]"):
    return base_size(typ[:-2])
  else:
    return {"int": 4, "char": 1, "char*": 4, "int*": 4}[typ]


def var_size(var):
  """ Returns the byte size for a given type (including array size) """
  count = var.count if var.typ.endswith("[]") else 1
  return base_size(var.typ) * count


class Definitions(object):
  """ Definitions track allocations (stack/global) during the lowering process.
  """

  def __init__(self):
    self._defs = []

  def push(self):
    self._l = len(self._defs)

  def pop(self):
    self._defs = self._defs[:self._l]

  def size(self, loc):
    size = 0
    for _, dsize, _, dloc, _ in self._defs:
      if dloc == loc:
        size += dsize
    return size

  def get(self, name):
    for dname, size, ofs, dloc, dtyp in self._defs:
      if dname == name:
        return dloc, ofs, base_size(dtyp)
    raise Exception("unknown var %s" % name)

  def defn(self, name, size, loc, typ):
    self._defs.append((name, size, self.size(loc), loc, typ))


# Next, define our register allocator. Allocation is (very) naive using R4-R6
# as temporaries. R0-R3 are arguments, R7 is a copy of the stack pointer (to
# allow us to access the SP using most Thumb instructions which only have R0-R7
# access).


R0, R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, R12, SP, LR, PC = range(16)


class Allocator(object):
  """ A very basic (bad) register allocator.
  """
  def __init__(self, spill, unspill):
    self._unused = [R4, R5, R6]
    self._alloced = []
    self._spilled = []
    self._spill, self._unspill = spill, unspill

  def get(self):
    """ Return an unused register """
    assert len(self._unused), "no free registers"
    reg = self._unused.pop(0)
    self._alloced.append(reg)
    return reg

  def put(self, tmp):
    """ Release a register (returning it to the unused pool) """
    if tmp in self._spilled:
      reg = self._spilled.pop()
      self._alloced.append(reg)
      self._unspill(reg)
    else:
      assert tmp not in self._unused
      self._unused = sorted(self._unused + [tmp])

  def spill_maybe(self, keep):
    """ If there are no unused registers, spill one (save it to the stack) to
        free up a register for use. If a register is spilled it is guaranteed
        not to be `keep`.
    """
    if len(self._unused) > 0:
      return
    for reg in self._alloced:
      if reg == keep:
        continue
      self._alloced.remove(reg)
      self._spilled.append(reg)
      self._spill(reg)
      assert reg not in self._unused
      self._unused = sorted(self._unused + [reg])
      return


# And now, define the the lowering visitor. Each visitor method maps to a
# matching IR Node. The `_lir` member is a list of LIR instructions which are
# `(opcode, argument, ...)` tuples. Each visitor method will emit one or more
# LIR instructions.


class Lower(object):
  # negated comparison operators for tests
  CMPN = {"<": ">=", ">": "<=", ">=": "<", "<=": ">", "==": "!=", "!=": "=="}
  # binary operations
  BINS = ["+", "-", "*", "/", "&", "<<", ">>", "|", "%"]

  def _visit(self, ir, *args):
    return getattr(self, "visit_" + ir.__class__.__name__)(ir, *args)

  def _visit_block(self, stmts, defs):
    for stmt in stmts:
      t = self._visit(stmt, defs)
      if t is not None:
        self._use_tmp(t)
        self._emit("drop", t)

  def _jump_cond(self, expr):
    if isinstance(expr.expr, IRBin) and expr.expr.op in self.CMPN:
      return self.CMPN[expr.expr.op]
    return "=="

  def _emit(self, *v):
    self._lir.append(v)
    return len(self._lir) - 1

  def _make_tmp(self):
    return self._allocator.get()

  def _use_tmp(self, tmp):
    return self._allocator.put(tmp)

  def _spill_maybe(self, keep):
    return self._allocator.spill_maybe(keep)

  def _make_label(self, n):
    self._label_count += 1
    return "%s%d" % (n, self._label_count)

  def lower(self, ir):
    self._lir = []
    self._label_count = 0
    self._allocator = Allocator(lambda reg: self._emit("push", reg),
                                lambda reg: self._emit("pop", reg))
    defs = Definitions()
    for glb in ir:
      self._visit(glb, defs)
    return self._lir

  def visit_Func(self, func, defs):
    self._infunc = True
    self._emit("lbl", func.name)
    p = self._emit("entr")
    defs.push()
    # create a single return label that all returns will jump to
    defs.retlbl = self._make_label("ret")
    # define our arguments and store each one as a local variable
    for i, (atyp, aname) in enumerate(func.args):
      defs.defn(aname, 4, "l", atyp)
      self._emit("stl", i, i * 4, 4) # store rN -> sp[N*4]
    # generate function body
    self._visit_block(func.body, defs)
    # generate return label and epilog (ret)
    self._emit("lbl", defs.retlbl)
    self._emit("ret", defs.size("l"))
    # fix up our prolog (entr) to have the correct number of locals
    self._lir[p] = ("entr", defs.size("l"))
    defs.pop()
    self._infunc = False

  def visit_Assign(self, assign, defs):
    te = self._visit(assign.expr, defs)
    if isinstance(assign.var, IRVar):
      self._visit(assign.var, defs)
      var = assign.var
      vloc, vofs, vtyp = defs.get(var.name)
      self._emit("st" + vloc, te, vofs, vtyp)
    elif isinstance(assign.var, IRName):
      name = assign.var
      vloc, vofs, vtyp = defs.get(name.v)
      self._emit("st" + vloc, te, vofs, vtyp)
    elif isinstance(assign.var, IRSubscript):
      subs = assign.var
      vloc, vofs, vtyp = defs.get(subs.name.v)
      ts = self._visit(subs.sub, defs)
      tv = self._make_tmp()
      if "[]" in subs.name.typ:
        self._emit("ad" + vloc, tv, vofs)
      else:
        self._emit("ld" + vloc, tv, vofs, vtyp)
      self._emit("bin", "+", tv, ts)
      self._emit("st", te, tv, vtyp)
      self._use_tmp(ts)
      self._use_tmp(tv)
    elif isinstance(assign.var, IRUnary):
      assert assign.var.op == "*"
      assert isinstance(assign.var.expr, IRName)
      vloc, vofs, vtyp = defs.get(assign.var.expr.v)
      tv = self._visit(assign.var.expr, defs)
      self._emit("st", te, tv, base_size(assign.expr.typ))
      self._use_tmp(tv)
    else:
      raise
    return te

  def visit_Unary(self, un, defs):
    if un.op == "*":
      assert isinstance(un.expr, IRName)
      if isinstance(un.expr, IRName):
        vloc, vofs, _ = defs.get(un.expr.v)
        tr = self._visit(un.expr, defs)
        self._emit("ld", tr, tr, base_size(un.typ))
        return tr
    elif un.op == "&":
      if isinstance(un.expr, IRName):
        tr = self._make_tmp()
        vloc, vofs, _ = defs.get(un.expr.v)
        self._emit("ad" + vloc, tr, vofs)
        return tr
      elif isinstance(un.expr, IRSubscript):
        tr = self._visit(un.expr.name, defs)
        ts = self._visit(un.expr.sub, defs)
        self._emit("bin", "+", tr, ts)
        self._use_tmp(ts)
        return tr
    elif un.op == "-":
      tr = self._visit(un.expr, defs)
      self._emit("neg", tr, tr)
      return tr
    raise

  def visit_Cast(self, cast, defs):
    """ Generating a cast is a noop in LIR as casts only matter for typing """
    return self._visit(cast.expr, defs)

  def visit_Test(self, test, defs):
    """ Generate a test (if/while) expression. Tests can take one of three forms
        - comparison: `if (a > b)`
        - other binop: `if (a & b)`
        - value: `if (a)`
        In the first case we emit a simple comparison, while the latter two, the
        implicit comparison against 0 is added
    """
    if isinstance(test.expr, IRBin):
      # if test is a binop, visit subexpressions as we would for a normal binop
      l = self._visit(test.expr.l, defs)
      self._spill_maybe(l)
      r = self._visit(test.expr.r, defs)
      # if the binop isn't a comparison (eg `if (a & b)`, emit 0 to compare to
      if test.expr.op in self.BINS:
        self._emit("bin", test.expr.op, l, r)
        self._use_tmp(r)
        r = self._make_tmp()
        self._emit("imm", r, 0)
    else:
      # if the test isn't a binop, visit it, then emit 0 to compare to
      l = self._visit(test.expr, defs)
      r = self._make_tmp()
      self._emit("imm", r, 0)
    # emit the final compare for the test
    self._emit("bin", "cmp", l, r)
    self._use_tmp(r)
    self._use_tmp(l)

  def visit_BinOp(self, binop, defs):
    l = self._visit(binop.l, defs)
    self._spill_maybe(l)
    r = self._visit(binop.r, defs)
    assert l != r
    op = binop.op if binop.op in self.BINS else "cmp"
    self._emit("bin", op, l, r)
    self._use_tmp(r)
    # if the binop is a comparison emit a compare-expression instruction that
    # sets the result to 1 or 0 depending on the compare result. That is,
    # `x = a > b` is effectively `x = (a > b) ? 1 : 0`
    if op == "cmp":
      self._emit("cmpe", binop.op, l)
    return l

  def visit_Var(self, var, defs):
    defs.defn(var.name, var_size(var), "l" if self._infunc else "g", var.typ)

  def visit_Name(self, name, defs):
    t = self._make_tmp()
    vloc, vofs, vtyp = defs.get(name.v)
    if name.typ.endswith("[]"):
      # Referring to an array directly (that is not via a subscript) is the same
      # as getting the address of that array.
      self._emit("ad" + vloc, t, vofs)
    else:
      # For all other variable we simply load their local/global address
      self._emit("ld" + vloc, t, vofs, vtyp)
    return t

  def visit_Num(self, num, defs):
    t = self._make_tmp()
    self._emit("imm", t, num.v)
    return t

  def visit_Return(self, retn, defs):
    if retn.expr is not None:
      t = self._visit(retn.expr, defs)
      self._emit("mov", R0, t)
      self._use_tmp(t)
    self._emit("jmp", defs.retlbl)

  def visit_If(self, ife, defs):
    elbl, dlbl = self._make_label("else"), self._make_label("done")
    self._visit(ife.test, defs)
    self._emit("jmpc", self._jump_cond(ife.test), elbl)
    self._visit_block(ife.tb, defs)
    self._emit("jmp", dlbl)
    self._emit("lbl", elbl)
    self._visit_block(ife.fb, defs)
    self._emit("lbl", dlbl)

  def visit_While(self, whil, defs):
    llbl, dlbl = self._make_label("loop"), self._make_label("done")
    self._emit("lbl", llbl)
    self._visit(whil.test, defs)
    self._emit("jmpc", self._jump_cond(whil.test), dlbl)
    self._visit_block(whil.body, defs)
    self._emit("jmp", llbl)
    self._emit("lbl", dlbl)

  def visit_Call(self, call, defs):
    # Emit code for evaluating all arguments pushing the values onto the stack.
    # Do this so we don't stomp on arguments with return values from nested
    # calls (FIXME: really should relying on the reg alloc for this).
    for arg in reversed(call.args):
      t = self._visit(arg, defs)
      self._emit("push", t)
      self._use_tmp(t)
    # Pop call arg values off the stack and assign to argument registers
    for i, arg in enumerate(call.args):
      t = self._make_tmp()
      self._emit("pop", t)
      self._emit("mka", t, i)
      self._use_tmp(t)
    tr = self._make_tmp()
    if call.func == "__svc":
      self._emit("bpkt", 0xab)
    else:
      self._emit("call", call.func)
    self._emit("mov", tr, R0)
    return tr

  def visit_Subscript(self, subs, defs):
    vloc, vofs, vtyp = defs.get(subs.name.v)
    ts = self._visit(subs.sub, defs)
    tv = self._make_tmp()
    if "[]" in subs.name.typ:
      self._emit("ad" + vloc, tv, vofs)
    else:
      self._emit("ld" + vloc, tv, vofs, vtyp)
    self._emit("bin", "+", tv, ts)
    self._emit("ld", tv, tv, vtyp)
    self._use_tmp(ts)
    return tv


def dump_lir(lir):
  """ Pretty-print a sequence of LIR instructions """
  for ins in lir:
    if ins[0] == "lbl":
      print "%s:" % ins[1]
    else:
      print "  %-4s %s" % (ins[0], ", ".join(map(str, ins[1:])))


# ## 5. Peephole Optimiser
#
# The peephole optimiser performs very basic LIR optimisation. It steps through
# LIR instructions in pairs and simplifies instruction sequences. This is mostly
# used to fix poor code generated by the lowering process (eg a jump to a label
# directly following the jump).
#
# The optimiser consists of match/replace rules of the form:
#
#     _ops_are("ld",   "mov") and _args_eq(1, 2): rep = (i0[0], i1[1])+i0[2:]
#
# which states, for two sequential instructions i0, i1, if (i0 is a "ld")
# and (i1 is a "mov") and (i0 arg1 equals i1 arg2) then replace the them with
# an instruction that is ("ld", i1 arg1, i0 arg2 ...)

# more specifically:
#
#     ("ld", 4, 0, 4) # ld  r4, [r4]
#     ("mov", 0, 4)   # mov r0, r4
#
# becomes:
#
#     ("ld", 0, 0, 4) # ld  r0, [r4]

# >>> peephole([('lbl', 'f'),
# >>>           ('entr', 4, 1),
# >>>           ('stl', 0, 0, 4),
# >>>           ('ldl', 4, 0, 4),
# >>>           ('mov', 0, 4),
# >>>           ('jmp', 'ret1'),
# >>>           ('lbl', 'ret1'),
# >>>           ('ret', 4)])
#     [('lbl', 'f'),
#      ('entr', 4, 1),
#      ('stl', 0, 0, 4),
#      ('ldl', 0, 0, 4),
#      ('lbl', 'ret1'),
#      ('ret', 4)]


def peephole(lir):
  """ Since a single pass of the peephole optimiser can result in LIR that could
      be further simplified, the optimiser is run until a fixed point is reached
  """
  while 1:
    orig_len = len(lir)
    lir = peephole_opt(lir)
    if len(lir) == orig_len:
      break
  return lir


def peephole_opt(lir):
  ret = []
  n = 0
  while n < len(lir):
    i0, i1 = lir[n], (lir[n + 1] if (n + 1) < len(lir) else ("drop", 0))
    n += 1

    # check if ins0 op == a and ins1 op == b
    def _ops_are(a, b):
      return i0[0] == a and i1[0] == b

    # check if ins0 arg[a] == ins1 arg[b]
    def _args_eq(a, b):
      return i0[a] == i1[b]

    # match/replace rules
    if   _ops_are("jmp",  "jmp"):                    rep = i0
    elif _ops_are("jmp",  "lbl") and _args_eq(1, 1): rep = i1
    elif _ops_are("mov", "drop") and _args_eq(1, 1): rep = ()
    elif _ops_are("ld",   "mov") and _args_eq(1, 2): rep = (i0[0], i1[1])+i0[2:]
    elif _ops_are("ldl",  "mov") and _args_eq(1, 2): rep = (i0[0], i1[1])+i0[2:]
    elif _ops_are("imm",  "mov") and _args_eq(1, 2): rep = (i0[0], i1[1])+i0[2:]
    elif _ops_are("imm",  "mka") and _args_eq(1, 1): rep = (i0[0], i1[2], i0[2])
    elif _ops_are("cmpe", "mka") and _args_eq(2, 1): rep = (i0[0], i0[1], i1[2])
    elif _ops_are("pop",  "mka") and _args_eq(1, 1): rep = (i0[0], i1[2])
    elif _ops_are("mov",  "mka") and _args_eq(1, 1): rep = (i1[0], i0[2], i1[2])
    elif _ops_are("mov",  "mov") and _args_eq(1, 2): rep = ("mov", i1[1], i0[2])
    elif _ops_are("push", "pop") and _args_eq(1, 1): rep = ()
    elif _ops_are("push", "pop"):                    rep = ("mov", i1[1], i0[1])
    else: rep = None

    # if we have a match, replace (ins0, ins1) with rep
    if rep is not None:
      if len(rep): ret.append(rep)
      n += 1
    else:
      ret.append(i0)
  if len(lir) and i1[0] != "drop":
    ret.append(i1)
  return ret


# ## 6. Codegen / Linker
#
# The code generator translates LIR into ARM Thumb2 machine code. LIR is already
# close to native machine code, so this process is mostly a 1:1 translation that
# emits 16bit words (thumb instructions). The codegen also builds a vector table
# to prepend to the resulting code necessary to support interrupts and allows
# the final binary to be flashed and booted directly on a microcontroller. The
# codegen also serves as a linker (usually a separate program in a real compiler
# as this allows separate compilation/linking of modules).

# >>> asm = Codegen().gen([('lbl', 'f'),
# >>>                      ('entr', 4, 1),
# >>>                      ('stl', 0, 0, 4),
# >>>                      ('ldl', 0, 0, 4),
# >>>                      ('lbl', 'ret1'),
# >>>                      ('ret', 4)])
# >>> import binascii
# >>> binascii.hexlify(asm[64:])   # skip 16 words vector table
#     80b581b06f463860386801b080bd # 14 bytes - 7 thumb instructions:
#                                  #   push {r7, lr}
#                                  #   sub  sp, #8
#                                  #   mov  r7, sp
#                                  #   str  r0, [r7, #0]
#                                  #   ldr  r0, [r7, #0]
#                                  #   add  sp, #8
#                                  #   pop  r7, pc}


def bit_extract(v, *fields):
  """ Extract bitfields from a number and return a list of bitfield values.
      Format is (width0, ofsset0) .. (widthN, osfsetN). """
  return [(v >> ofs) & ((1 << width) - 1) for width, ofs in fields]


class Codegen(object):
  # ARM condition codes (used for conditional jumps)
  EQ, NE, CS, CC, MI, PL, VS, VC, HI, LS, GE, LT, GT, LE = range(14)
  # mapping from LIR comparisons -> condition codes
  CMPS = {"==": EQ, "!=": NE, ">": GT, ">=": GE, "<": LT, "<=": LE}

  def __init__(self, vecttbl, sp, base):
    # ARM Thumb2 instructions are almost all 16bit, so our generated code array
    # is a list of of 16bit values. The first 16+ 32bit values in an Cortex M
    # Flash image are the vector table, so we reserve space for that before
    # emitting code.
    self.code = [0] * 2 * (vecttbl + 16) # reserve space for vector table
    self.sp = sp
    self.base = base

  # Raw code emitters

  def _gen16(self, v):
    self.code.append(v)

  def _gen32(self, v):
    self._gen16(v >> 16)
    self._gen16(v & 0XFFFF)

  # Miscellaneous instruction generation support. Naming key:
  # xxx_r   - register <- register
  # xxx_ri  - register <- register, immediate
  # xxx_spi - stack ptr <- stack ptr, immediate

  def _make_jump(self, target_addr, addr):
    """ Construct a 32bit jump instruction (24bit offset): see A6.7.12 (T4) B
    """
    ofs = target_addr - addr - 4
    ofs = ofs if ofs >= 0 else (-ofs-1) ^ 0xFFFFFFFF
    imm11, imm10, i2, i1, s = bit_extract(ofs, (11, 1), (10, 12), (1, 22),
                                               (1, 23), (1, 24))
    i1 = 1 - (i1 ^ s)
    i2 = 1 - (i2 ^ s)
    return (s<<26) | (imm10<<16) | (i1<<13) | (i2<<11) | (imm11)

  def _imm32(self, base, rd, imm):
    """ Construct a 32bit immediate instruction (16bit immediate): see A6.7.75
    """
    i8, i3, i1, i4 = bit_extract(imm, (8, 0), (3, 8), (1, 11), (4, 12))
    self._gen32(base | (i1<<26) | (i4<<16) | (i3<<12) | (rd<<8) | (i8))

  def _arith_i(self, base, rdn, imm):
    """ Register-immediate arithmetic: rdn = rdn OP imm """
    self._gen16(base | (rdn<<8) | (imm))

  def _arith_r(self, base, rdm, rn):
    """ Register-register arithmetic: rdm = rdm OP rn """
    self._gen16(base | (rn<<3) | (rdm))

  def _arith_rr(self, base, rd, rn, rm):
    """ Register-register-register arithmetic: rd = rm OP rn """
    self._gen16(base | (rm<<6) | (rn<<3) | (rd))

  def _arith_spi(self, base, imm):
    """ SP-immediate arithmetic: SP = SP OP imm """
    self._gen16(base | (imm>>2))

  # Instructions

  def _push_reglist(self, *regs):
    """ Push a list of registers onto the stack """
    reglist_bits = sum((1<<i) for i in regs if i < 8)
    lr_bit = (0x100 if LR in regs else 0)
    self._gen16(0xB400 | lr_bit | reglist_bits) # A6.7.98 (T1) PUSH

  def _pop_reglist(self, *regs):
    """ Pop from the stack into a list of registers """
    reglist_bits = sum((1<<i) for i in regs if i < 8)
    pc_bit = (0x100 if PC in regs else 0)
    self._gen16(0xBC00 | pc_bit | reglist_bits) # A6.7.97 (T1) POP

  def _mov_r(self, rd, rs):
    """ Move register rs into rd """
    self._gen16(0x4600 | (rd) | (rs<<3)) # A6.7.76 (T1) MOV

  def _mov_i(self, rd, imm):
    """ Move an immediate value into register rd """
    assert imm >= 0
    if (rd < R8) and (imm < 0x100):
      self._gen16(0x2000 | (rd<<8) | abs(imm))  # A6.7.75 (T1) MOV
    elif imm < 0x10000:
      self._imm32(0xF2400000, rd, abs(imm))     # A6.7.75 (T3) MOV
    elif imm < 0x100000000:
      # use most compact form for low 16 bits then MOVT for high 16
      self._mov_i(rd, imm & 0xFFFF)
      self._imm32(0xF2C00000, rd, imm >> 16)    # A6.7.78 (T1) MOVT
    else:
      raise Exception("Integer literal too large")

  def _neg_r(self, rd, rs):
    """ Assign register rd the negated value in rs """
    self._gen16(0x4240 | (rs<<3) | (rd)) # A6.7.106 (T1) RSB

  def _str_ri(self, sz, rt, rn, imm):
    """ Store register rt (sized sz) into memory at address in rn + immediate
        imm """
    assert sz in [1, 4], ("Unsupported store size: %d" % sz)
    if sz == 4:
      assert imm < 128
      self._gen16(0x6000 | (rt) | (rn<<3) | ((imm/4)<<6)) # A6.7.119 (T1) STR
    elif sz == 1:
      assert imm < 32
      self._gen16(0x7000 | (rt) | (rn<<3) | (imm<<6)) # A6.7.121 (T1) STRB

  def _st_global(self, sz, rs, addr):
    """ Store register rs (sized sz) into memory at address addr """
    self._mov_i(R8, addr + self.base)
    assert sz in [1, 4], ("Unsupported store size: %d" % sz)
    if sz == 4:
      self._gen32(0xF8C00000 | (rs<<12) | (R8<<16)) # A6.7.119 (T3) STR
    elif sz == 1:
      self._gen32(0xF8800000 | (rs<<12) | (R8<<16)) # A6.7.121 (T2) STR

  def _ldr_ri(self, sz, rt, rn, imm):
    """ Load value (sized sz) in memory at address in rn + immediate imm into rt
    """
    assert sz in [1, 4], ("Unsupported load size: %d" % sz)
    if sz == 4:
      assert imm < 128
      self._gen16(0x6800 | (rt) | (rn<<3) | ((imm / 4)<<6)) # A6.7.42 (T1) LDR
    elif sz == 1:
      assert imm < 32
      self._gen16(0x7800 | (rt) | (rn<<3) | (imm<<6)) # A6.7.45 (T1) LDRB

  def _ld_global(self, sz, rt, addr):
    """ Load value (sized sz) in memory at address addr into register rt
    """
    self._mov_i(rt, addr + self.base)
    self._ldr_ri(sz, rt, rt, 0)

  def _jmpc(self, cond, ofs):
    """ Conditional jump (condition cond) to relative offset ofs """
    assert -128 <= ofs < 128
    self._gen16(0xD000 | (cond<<8) | ofs) # A6.7.12 (T1) B<c>

  def _jmpu(self, ofs):
    """ Unconditional jump to relative offset ofs """
    assert -1024 <= ofs < 1024
    self._gen16(0xE000 | ofs) # A6.7.12 (T2) B

  def _call(self, addr):
    """ Call function at address addr """
    ofs = self._make_jump(addr, len(self.code) * 2)
    self._gen32(0xF000D000 | ofs) # A6.7.18 (T1) BL

  def _bpkt(self, arg):
    """ Trigger a software breakpoint of value arg """
    self._gen16(0xBE00 | arg) # A6.7.17 (T1) BPKT

  def _addr_loc(self, rd, ofs):
    """ Load the address of a local into register rd (SP + ofs) """
    self._mov_r(rd, R7)
    if ofs > 0:
      self._arith_i(0x3000, rd, ofs) # A6.7.3 (T2) ADD

  def _div_rr(self, rd, rn, rm):
    """ Signed integer division: rd = rn // rm """
    self._gen32(0xFB90F0F0 | (rn<<16) | (rd<<8) | (rm)) # A.6.7.111 (T1) SDIV

  def _mod_rr(self, rd, rn, rm):
    """ Modulo division (%) implementation: rd = rn % rm """
    # n % m == n - (n // m) * m
    # udiv r8,rn,rm    : r8 = rn / rm          A6.7.145 (T1) UDIV,
    # mls  rd,r8,rm,rn : rd = rn - (r8 * rm)   A6.7.74 (T1) MLS
    self._gen32(0xFBB0F0F0 | (rn<<16) | (R8<<8) | (rm))
    self._gen32(0xFB000010 | (rd<<8) | (R8<<16) | (rm) | (rn<<12))

  def _cmpe(self, op, rd):
    """ Comparison expression rd = (x ? 1 : 0) using IT instruction """
    # ITE mask = (not firstcond[0]) 1 0 0
    mask = 4 | ((not (self.CMPS[op] & 1)) << 3)
    self._gen16(0xBF00 | (self.CMPS[op] << 4) | mask) # A6.7.37 (T1) IT
    self._mov_i(rd, 1) # executed if comparison is true
    self._mov_i(rd, 0) # executed if comparison is false

  def _binop(self, op, rd, rs):
    """ Generate a binary (arithmetic/comparison) operation """
    # mapping 2 arg binop instructions -> opcodes A6.7.[68,70,9,91,93,28]
    bins = {"<<": 0x4080, ">>": 0x40A0, "&": 0x4000, "|": 0x4300, "*": 0x4340,
            "cmp": 0x4280}
    # First check for 3 arg binop instructions, then fall back to 2 args
    if   op == "+":  self._arith_rr(0x1800, rd, rd, rs)
    elif op == "-":  self._arith_rr(0x1A00, rd, rd, rs)
    elif op == "/":  self._div_rr(rd, rd, rs)
    elif op == "%":  self._mod_rr(rd, rd, rs)
    elif op in bins: self._arith_r(bins[op], rd, rs)
    else:            raise Exception("Unhandled binop: %s" % op)

  def _enter(self, local):
    """ Function prolog: push scratch registers and link register, make space
        on stack for locals, and copy SP to R7 """
    self._push_reglist(R4, R5, R6, R7, LR)
    if local:
      self._arith_spi(0xb080, local) # A6.7.134 (T1) SUB
    self._mov_r(R7, SP)

  def _return(self, local):
    """ Function epilog: restore stack ('free' locals), pop scratch registers
        and PC """
    if local:
      self._arith_spi(0xb000, local) # A6.7.5 (T2) ADD
    self._pop_reglist(R4, R5, R6, R7, PC)

  # Vector table

  def _write_vect32(self, n, v):
    self.code[(n * 2) + 1] = v >> 16
    self.code[(n * 2)    ] = v & 0xffff

  def _build_vectors(self, labels, sp=0):
    """ Build the vector table in our resulting binary (in the space saved
        before the code - see `__init__`). The vector table contains initial
        stack pointer, reset vector (code entry point) and exception handlers
    """
    if "main" not in labels:
      raise Exception("no 'main' function")

    # exception handler function name -> vector table index mapping
    exceptions = {"nmi_handler": 2, "hardfault_handler": 3, "svc_handler": 11,
                  "pendsv_handler": 14, "systick_handler": 15}
    # Vector table starts with initial stack pointer value, followed by a set of
    # exception/interrupt handler addresses (low bit set high to indicate thumb)
    self._write_vect32(0, sp)
    for label, addr in labels.items():
      jmp = addr | 1
      if label.startswith("interrupt_handler"):     # CPU-specific IRQs
        intr = int(label[17:]) + 16
        self._write_vect32(intr, jmp)
      elif label in exceptions:                     # Cortex exception handlers
        self._write_vect32(exceptions[label], jmp)
      elif label == "main":                         # reset vector
        self._write_vect32(1, jmp)

  # Core generator

  def gen(self, lir):
    """ Core code generator. Iterates through LIR instructions and emits
        corresponding ARM Thumb instructions (potentially more than one ARM ins
        per LIR ins).
    """
    # To calculate the correct jump offset for forward jumps we initially emit
    # an "empty" jump (incorrect offset), then come back once we reach the jump
    # target (and thus know the offset) to fix up the jump.
    fixups = []
    labels = {}

    def fixup(typ, label):
      fixups.append((label, typ, (len(self.code) - 1) * 2))

    # translate LIR instructions to machine code
    for ins in lir:
      op, args = ins[0], ins[1:]
      if   op == "drop": pass
      elif op == "lbl":  labels[args[0]] = len(self.code) * 2
      elif op == "entr": self._enter(args[0])
      elif op == "ret":  self._return(args[0])
      elif op == "push": self._push_reglist(args[0])
      elif op == "pop":  self._pop_reglist(args[0])
      elif op == "ld":   self._ldr_ri(args[2], args[0], args[1], 0)
      elif op == "ldl":  self._ldr_ri(args[2], args[0], R7, args[1])
      elif op == "ldg":  self._ld_global(args[2], args[0], args[1])
      elif op == "st":   self._str_ri(args[2], args[0], args[1], 0)
      elif op == "stl":  self._str_ri(args[2], args[0], R7, args[1])
      elif op == "stg":  self._st_global(args[2], args[0], args[1])
      elif op == "mov":  self._mov_r(args[0], args[1])
      elif op == "imm":  self._mov_i(args[0], args[1])
      elif op == "mka":  self._mov_r(args[1], args[0])
      elif op == "call": self._call(labels[args[0]])
      elif op == "bpkt": self._bpkt(args[0])
      elif op == "adl":  self._addr_loc(args[0], args[1])
      elif op == "adg":  self._mov_i(args[0], args[1] + self.base)
      elif op == "jmp":  self._jmpu(0); fixup("ju", args[0])
      elif op == "jmpc": self._jmpc(self.CMPS[args[0]], 0); fixup("jc", args[1])
      elif op == "neg":  self._neg_r(args[0], args[1])
      elif op == "cmpe": self._cmpe(args[0], args[1])
      elif op == "bin":  self._binop(args[0], args[1], args[2])
      else:              raise Exception("unhandled lir: %s" % str(ins))

    # The 'linker':
    # Iterate through fixups and patch any jump/branch with the correct offset
    for (fixlabel, fixtyp, addr) in fixups:
      target_addr = labels[fixlabel]
      ofs = (target_addr - addr - 4) / 2
      assert -128 <= ofs < 128, "relative jump out of range"
      ofs = ofs if ofs >= 0 else (-ofs - 1) ^ 0x7FF # two's complement
      self.code[addr / 2] |= ofs

    # build vector table
    self._build_vectors(labels, sp=self.sp)

    # generate resulting byte array
    code_bin = bytearray()
    for ins in self.code:
      code_bin.append(ins & 0xff)
      code_bin.append(ins >> 8)
    return code_bin


# ## 7. Driver
#
# The driver ties together the six stages passing the output of one to the input
# of the next. The input is a string read the source file, and the final output
# is a (binary) string written to be written to the output binary file. Input
# sources are first run through CPP to preprocess includes and defines.

# >>> repr(driver("int f() { return 1; }"))
#     '.... \x80\xb5oF\x01 \x80\xbd' # prepended by vector table

def driver(src):
  """ Compile the given C source and return an executable ARM binary image """
  lex = Lexer(src)
  ir = Parser(lex).parse()
  ir = Semantic().typecheck(ir)
  lir = Lower().lower(ir)
  lir = peephole(lir)
  # dump_lir(lir)
  code = Codegen(vecttbl=32, sp=0x20002000, base=0x20000000).gen(lir)
  return code


def preprocess(src_fn):
  """ We cheat and use CPP (GNU C Preprocessor) to preprocess the source """
  cpp = ["cpp", "-I", "lib", "-nostdinc", "-P", src_fn]
  return subprocess.check_output(cpp)


def main(src_fn, bin_fn):
  try:
    out = driver(preprocess(src_fn))
    open(bin_fn, "wb").write(out)
    return 0
  except Exception as e:
    print "Compile failed: %s" % e
    # import traceback; traceback.print_exc()
    return -1


if __name__ == "__main__":
  src_fn = sys.argv[1]
  bin_fn = src_fn.split(".")[0]+".bin"
  sys.exit(main(src_fn, bin_fn))
